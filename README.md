# Campaign 1

## For Players
### Goal
Build a fun and interesting story.

### Guidelines
The following are guidelines and don't have to be followed 100% of the time. The lists of **Possible Exceptions** are not exhaustive.

1. When a player offers a suggestion or explanation, go along with it and do this sincerely. Play along even when it goes against your character. "Yes, and..." is a staple in improv, look it up and try it out once in a while. Once you say yes to the idea, explore it with the player who suggested it. Develop the idea further until you either accept the idea and act on it, or toss the idea. You don't have to roleplay likeing the idea, just accept it as a valid idea and be supportive of the person who thought of it. These experiences allow you to explore how your character would deal with certain consequences while showing other players who your character is. You will never be penalized for developing your character in this way.   
    **Possible Exceptions**   
    - Feel free to object to actions that might alter your alignment   
    - Feel free to play devils advocate when developing the idea further after the initial agreement   
    - You don't have to agree with everything. Though, disagreements should be the exception 
2. Try not to talk over each other. Listen and attempt to recognize where other players are trying to bring the story. Don't interrupt once you figure it out. Wait your turn and assist them in driving the story.   
    **Possible Exceptions**  
    - There are plenty of role play scenarios where it is perfectly fine to yell over eachother.
3. Player conflict should generally be for comedic purposes.   
    **Possible Exceptions**  
    - For any other reason, you should discuss with the other player out of session and agree on something. If it's something simple you can even send a text to get the answer during the session.

#### Lesser Guidelines
1. Don't get consumed by the consequences of past actions or potential future outcomes. There is **no** doomsday clock even when there seems to be. Bad things might happen if you choose not to act. Bad things might happen if you choose to act. Or, good things might happen in either of these scenarios. Your characters still have the ability to affect positive change but you do not have complete control over every scenario.   
    **Possible Exceptions**   
    - If you want to role play a particular scenario as extremely important to your character, by all means, have fun and do it. Just try not to treat every scenario as such. This can lead to multiple players having conflicting desires and often devolves into an argument which is rarely fun to be a part of.      

### Rules
The following are rules and should be followed 100% of the time (unless the DM says otherwise).

1. If there is a dispute over 5e rules, the DM will choose one of the presented options to use for the rest of the session. The DM will solve the dispute in-between sessions. Don't worry about looking up the rules mid session.
